<!DOCTYPE html>
<html lang=en>
   <head>
      <meta charset="UTF-8">
      <title>Kafka Tutorial #3</title>
      <link rel="stylesheet" type="text/css" media="all" href="style.css">
   </head>
   <body>
      <h1>Kafka Tutorial #3: Cluster and replicas</h1>
      <p>Kafka can be used on a single machine simply to connect services. And a single Kafka server is often enough to try out a software solution. But in many (most) real-world applications there are advantages to using multiple brokers organized as a cluster: you can balance the load across the servers to meet the application needs in term of CPU, memory or storage resources; and you can use replication to avoid data loss in case a broker fails.</p>
      <h2>In a nutshell:</h2>
      <ul>
         <li>Each partition can have multiple replicas. Partitions and replicas can be on the same server but to ensure tolerance to failure they are usually stored on different machines.</li>
         <li>When you create a topic from the command-line, you can specify the number of replicas (3 for instance) using the <code>--replication-factor 3</code> option.</li>
         <li>Each partition has a replica designated as the <i>leader</i> while the others are called <i>followers</i>.</li>
         <li>All production and consumption operations are performed on the leader. Followers don't serve client requests; they stay in sync with the leader and if it crashes, one of the followers gets elected the new leader.</li>
         <li>When a Kafka server starts, it registers with its unique id in Zookeeper.</li>
         <li>The first server, in addition to be a regular broker, gets to be the <i>controller</i> of the cluster. As such, it is responsible for choosing partition leaders or changing them when it detects brokers which leave or join the cluster. Mechanisms exist to ensure that the cluster always have one and only one controller.</li>
      </ul>
      <p>Figures from "Kafka: The Definitive Guide":</p>
      <figure>
         <img src="replicas.png" alt="replication of partitions in a cluster" width="594" height="324"><br>
         <figcaption>Replication of partitions in a cluster</figcaption>
      </figure>
      <figure>
         <img src="cluster.png" alt="a simple Kafka cluster" width="453" height="341"><br>
         <figcaption>A simple Kafka cluster</figcaption>
      </figure>
      <h2>Experiment</h2>
      <p>We will follow the example presented in <a href="https://kafka.apache.org/24/documentation.html#quickstart">the Kafka quickstart documentation</a> about setting up a multi-broker cluster (section 6).</p>
      <p>So far, when starting a broker, we've used the default configuration from the file in Kafka installation folder: <code>$KAFKA_HOME/config/server.properties</code>. Among other things, this file specifies the ID of the broker, its associated TCP port, and the name of the log file. That information can't be the same for two different brokers so it is not possible to start a second broker using the same configuration file. We copy the default configuration file and edit it to set different values for our second broker:</p>
      <ol>
      <li><code>cp $KAFKA_HOME/config/server.properties server-1.properties</code></li>
      <li><code>gedit server-1.properties</code></li>
      <li><ul>
         <li>Line 24, "broker.id=0" is changed into <code>broker.id=1</code></li>
         <li>Line 34, "#listeners=PLAINTEXT://:9092" is uncommented and a different port is chosen: <code>listeners=PLAINTEXT://:9093</code></li>
         <li>Line 62, "log.dirs=/tmp/kafka-logs" is changed into <code>log.dirs=/tmp/kafka-logs-1</code></li>
      </ul></li>
      </ol>
       
      <p>If Zookeeper and the first broker are not started yet, start them as usual:<br>
	<code>$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties &</code><br>
	<code>$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties &</code></p> 
      
      <p>Start the second broker using the new configuration file:<br>
      <code>$KAFKA_HOME/bin/kafka-server-start.sh server-1.properties &</code></p>
      
      <p>Create a new configuration file "server-2.properties" and launch a third broker.</p>
      
      
      <p><i>Create a new topic with a replication factor of 2:</i><br>
      <code>$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092,localhost:9093 --replication-factor 2 --partitions 1 --topic testtuto3</code><br>
      note: since I'll kill one broker I provide a second broker in the bootstrap server list so at least one is reachable)</p>
      
      <p><i>Okay but now that we have a cluster how can we know which broker is doing what? To see that run the "describe topics" command:</i><br>
      <code>$KAFKA_HOME/bin/kafka-topics.sh --describe --bootstrap-server localhost:9092,localhost:9093 --topic testtuto3</code><br>
      <pre>Topic: testtuto3	TopicId: Itccv7FRSzyF8oEeBCWjjg	PartitionCount: 1	ReplicationFactor: 2	Configs: 
	Topic: testtuto3	Partition: 0	Leader: 1	Replicas: 1,2	Isr: 1,2</pre><br>
      <i>Here is an explanation of output. The first line gives a summary of all the partitions, each additional line gives information about one partition. Since we have only one partition for this topic there is only one line.</i><br>
    <i>"leader"</i> (1 in the output above) <i>is the node responsible for all reads and writes for the given partition. Each node will be the leader for a randomly selected portion of the partitions.</i><br>
    <i>"replicas" is the list of nodes that replicate the log for this partition regardless of whether they are the leader or even if they are currently alive.</i><br>
    <i>"isr" is the set of "in-sync" replicas. This is the subset of the replicas list that is currently alive and caught-up to the leader.</i></p>
    
      <p><i>Let's publish a few messages to our new topic: </i><br>
      <code>$KAFKA_HOME/bin/kafka-console-producer.sh --broker-list localhost:9092,localhost:9093 --topic testtuto3</code><br>
      <i>Now let's consume these messages:</i><br>
      <code>$KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092,localhost:9093 --from-beginning --topic testtuto3</code></p>
      
      <p><i>Now let's test out fault-tolerance. Broker 1 was acting as the leader so let's kill it:</i><br>
      The <code>pgrep -f</code> command returns the processus id of the commands using a given parameter. If the leader in your case is also broker 1, use the command <code>pgrep -f "server-1.properties"</code>. If the leader in your case is brocker 2, use the command <code>pgrep -f "server-2.properties"</code>. If the leader in your case is brocker 0, use the command <code>pgrep -f "server.properties"</code>.
<br>
        <pre>pgrep -f "server-1.properties"
1338
</pre>
	Use the <code>kill</code> command with the appropriate pid to kill the process:<br>
	<code>kill 1338</code></p>
	
	<p><i>Leadership has switched to the follower and node 1 is no longer in the in-sync replica set</i>:<br>
	<pre>$KAFKA_HOME/bin/kafka-topics.sh --describe --bootstrap-server localhost:9092,localhost:9093 --topic testtuto3
Topic: testtuto3	TopicId: Itccv7FRSzyF8oEeBCWjjg	PartitionCount: 1	ReplicationFactor: 2	Configs: 
	Topic: testtuto3	Partition: 0	Leader: 2	Replicas: 1,2	Isr: 2</pre></p>
      
      <p>Check that you can still produce messages to and read from the topic.</p>
   </body>
</html>

