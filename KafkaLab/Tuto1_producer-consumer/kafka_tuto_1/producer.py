from kafka import KafkaProducer
from time import sleep

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

for i in range(10):
    message = "message number" + str(i)
    producer.send(topic='test', value=message.encode("utf-8"))
    print("sent:" + message)
    sleep(1)

