<!DOCTYPE html>
<html lang=en>
   <head>
      <meta charset="UTF-8">
      <title>Kafka Tutorial #2</title>
      <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/styles/default.min.css">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.0.3/highlight.min.js"></script>
      <script>hljs.initHighlightingOnLoad();</script>
   </head>
   <body>
      <h1>Kafka Tutorial #2: Balance partitions between consumers</h1>
      <h2>Partitions and consumers: main concepts</h2>
      <p>Read this extract from "<em><a href="https://www.oreilly.com/library/view/kafka-the-definitive/9781491936153/">Kafka: The Definitive Guide</a></em>", a book by Neha Narkhede, Gwen Shapira, and Todd Palino:</p>
      <hr>
      <p>Suppose you have an application that needs to read messages from a Kafka topic, run some validations against them, and write the results to another data store. In this case your application will create a consumer object, subscribe to the appropriate topic,
         and start receiving messages, validating them and writing the results. This may work well for a while, but what if the rate at which producers write messages to the topic exceeds the rate at which your application can validate them? If you are limited
         to a single consumer reading and processing the data, your application may fall farther and farther behind, unable to keep up with the rate of incoming messages. Obviously there is a need to scale consumption from topics. Just like multiple producers
         can write to the same topic, we need to allow multiple consumers to read from the same topic, splitting the data between them.
      </p>
      <p>Kafka consumers are typically part of a consumer group. When multiple consumers are subscribed to a topic and belong to the same consumer group, each consumer in the group will receive messages from a different subset of the partitions in the topic.</p>
      <p>Let’s take topic T1 with four partitions. Now suppose we created a new consumer, C1, which is the only consumer in group G1, and use it to subscribe to topic T1. Consumer C1 will get all messages from all four T1 partitions. See Figure 4-1.</p>
      <figure>
         <img src="ktdg_04in01.png" alt="one consumer group with 4 partitions" width="300"><br>
         <figcaption>Figure 4-1. One Consumer group with four partitions</figcaption>
      </figure>
      <p>If we add another consumer, C2, to group G1, each consumer will only get messages from two partitions. Perhaps messages from partition 0 and 2 go to C1 and messages from partitions 1 and 3 go to consumer C2. See Figure 4-2.</p>
      <figure>
         <img src="ktdg_04in02.png" alt="Four partitions split to two consumers in a group" width="300"><br>
         <figcaption>Figure 4-2. Four partitions split to two consumers in a group</figcaption>
      </figure>
      <p>If G1 has four consumers, then each will read messages from a single partition. See Figure 4-3.</p>
      <figure>
         <img src="ktdg_04in03.png" alt="Four consumers in a group with one partition each" width="300"><br>
         <figcaption>Figure 4-3. Four consumers in a group with one partition each</figcaption>
      </figure>
      <p>If we add more consumers to a single group with a single topic than we have partitions, some of the consumers will be idle and get no messages at all. See Figure 4-4.</p>
      <figure>
         <img src="ktdg_04in04.png" alt="More consumers in a group than partitions means idle consumers" width="300"><br>
         <figcaption>Figure 4-4. More consumers in a group than partitions means idle consumers</figcaption>
      </figure>
      <p>The main way we scale data consumption from a Kafka topic is by adding more consumers to a consumer group. It is common for Kafka consumers to do high-latency operations such as write to a database or a time-consuming computation on the data. In these
         cases, a single consumer can’t possibly keep up with the rate data flows into a topic, and adding more consumers that share the load by having each consumer own just a subset of the partitions and messages is our main method of scaling. This is a
         good reason to create topics with a large number of partitions—it allows adding more consumers when the load increases. Keep in mind that there is no point in adding more consumers than you have partitions in a topic—some of the consumers will just
         be idle.
      </p>
      <p>[...]</p>
      <p>In addition to adding consumers in order to scale a single application, it is very common to have multiple applications that need to read data from the same topic. In fact, one of the main design goals in Kafka was to make the data produced to Kafka topics
         available for many use cases throughout the organization. In those cases, we want each application to get all of the messages, rather than just a subset. To make sure an application gets all the messages in a topic, ensure the application has its
         own consumer group. Unlike many traditional messaging systems, Kafka scales to a large number of consumers and consumer groups without reducing performance.
      </p>
      <p>In the previous example, if we add a new consumer group G2 with a single consumer, this consumer will get all the messages in topic T1 independent of what G1 is doing. G2 can have more than a single consumer, in which case they will each get a subset
         of partitions, just like we showed for G1, but G2 as a whole will still get all the messages regardless of other consumer groups. See Figure 4-5.
      </p>
      <figure>
         <img src="ktdg_04in05.png" alt="Adding a new consumer group, both groups receive all messages" width="300"><br>
         <figcaption>Figure 4-5. Adding a new consumer group, both groups receive all messages</figcaption>
      </figure>
      <p>To summarize, you create a new consumer group for each application that needs all the messages from one or more topics. You add consumers to an existing consumer group to scale the reading and processing of messages from the topics, so each additional
         consumer in a group will only get a subset of the messages.
      </p>
      <hr>
      <h2>Hand-on experiments</h2>
      
      <p>If they are not started yet, start Zookeeper and a Kafka broker:<br>
        <code>$KAFKA_HOME/bin/zookeeper-server-start.sh $KAFKA_HOME/config/zookeeper.properties &amp;</code><br>
        <code>$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties &amp;</code>
      </p>

      <p>Create a topic named "test" <b>with three partitions</b>:<br>
         <code>$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 3 --topic test</code>
      </p>
      <p>Note:</p>
      <ul>
         <li>You can check that the topic has three partitions by displaying its properties:<br>
            <code>$KAFKA_HOME/bin/kafka-topics.sh --describe --bootstrap-server localhost:9092 --topic test</code><br>
            <pre>Topic: test	TopicId: K896LVSAQ6-flC6Apmy7Fw	PartitionCount: 3	ReplicationFactor: 1	Configs: segment.bytes=1073741824
	Topic: test	Partition: 0	Leader: 0	Replicas: 0	Isr: 0
	Topic: test	Partition: 1	Leader: 0	Replicas: 0	Isr: 0
	Topic: test	Partition: 2	Leader: 0	Replicas: 0	Isr: 0</pre>
         </li>
         <li>If you list files in the default Kafka log location (<code>/tmp/kafka-logs</code>, it can be changed in the <code>server.properties</code> file or using <code>--override</code> on the launching command line), you will find 3 repositories: <code>test-0</code>, <code>test-1</code> and <code>test-2</code>; each repository is associated with a partition.</li>
      </ul>  
      
      <p>Create a verbose producer that sends records to the topic every second. It uses  <a href="https://github.com/dpkp/kafka-python/blob/master/kafka/producer/future.py"><code>FutureRecordMetadata</code></a> to get information on the outcome of the <code>send</code> and it prints the id of the partition the record was sent to:</p>
      <pre><code class="python">from kafka import KafkaProducer
from time import sleep

producer = KafkaProducer(bootstrap_servers=['localhost:9092'])

for i in range(10000):
    message = "message number" + str(i)
    partition_id = producer.send(topic='test', value=message.encode("utf-8")).get().partition
    print("sent:" + message + " to partition " + str(partition_id))
    sleep(1)
      </pre></code>
      
      <p>Create a verbose consumer that reads from the topic and prints the record read, as well as the partition it was read from:</p>
      <pre><code class="python">from kafka import KafkaConsumer

consumer = KafkaConsumer('test',
                         bootstrap_servers=['localhost:9092'], 
                         auto_offset_reset='earliest',
                         group_id='python-group')

try:
    for message in consumer:
        print("received: " + message.value.decode("utf-8") + " from partition " + str(message.partition))

except KeyboardInterrupt:
    pass

finally:
    consumer.close()</pre></code>
    
      <p>Start the producer and consumer.<br>
      Observe that the consumer is reading records from all 3 partitions.</p>
     <br>
      <p>Run an additional consumer.<br>
         Observe that after a short while, one consumer reads from 2 of the partitions only while the other reads from the remaining partition only.</p>
      <br>
      <p>Run a third consumer.<br>
         Observe that after a short while, each consumer reads from a separate partition. During rebalancing, partitions sometimes get reassigned to a different consumer (but processes are informed before the reassignement takes place, and can take appropriate measures such a storing their current state so it can be restored by the process that will get assigned to the partitions they're about to "lose").</p>
      <br>
      <p>Run a fourth consumer.<br>
         Observe that there is still one consumer per partition while a fourth one gets nothing.</p>
      <br>
      <p>Change the topic configuration to add a fourth partition:<br>
         <code>$KAFKA_HOME/bin/kafka-topics.sh --bootstrap-server localhost:9092 --alter --topic test --partitions 4</code><br>
         At first, it seems that the producer is not picking the new partition. It takes 5 minutes before it starts publishing on partition 3 (the fourth one) and the starving consumer gets data.<br>
         That's because the default value for <a href="https://kafka.apache.org/documentation/#metadata.max.age.ms"><code>metadata.max.age.ms</code></a>, the period of time after which we force a refresh of metadata even if we haven't seen any partition leadership changes to proactively discover any new brokers or partitions, is 300000 milliseconds. You can use these 5 minutes to read the "Additional info" section below.</p>
      <br>
      <p>Stop two of the consumers.<br>
         Observe that (after a little while) the remaining consumers each read from a separate pair of partitions.</p>
      <br>
      <p>Optionally, run the experiments again with a different value for the producer and consumer <code>metadata.max.age.ms</code> properties:</p>
      <p><pre><code class="python">producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         metadata_max_age_ms=20000)</pre></code></p>
      <p><pre><code class="python">consumer = KafkaConsumer('test',
                         bootstrap_servers=['localhost:9092'], 
                         auto_offset_reset='earliest',
                         group_id='python-group',
                         metadata_max_age_ms=20000)</pre></code></p>
                         
      <h2>Additional info from "Kafka: The Definitive Guide"</h2>
      <hr>
      <p>Consumers in a consumer group share ownership of the partitions in the topics they subscribe to. When we add a new consumer to the group, it starts consuming messages from partitions previously consumed by another consumer. The same thing happens when a consumer shuts down or crashes; it leaves the group, and the partitions it used to consume will be consumed by one of the remaining consumers. Reassignment of partitions to consumers also happen when the topics the consumer group is consuming are modified (e.g., if an administrator adds new partitions). [...]</p>
      <p>Moving partition ownership from one consumer to another is called a <em>rebalance</em>. Rebalances are important because they provide the consumer group with high availability and scalability (allowing us to easily and safely add and remove consumers), but in the normal course of events they are fairly undesirable. During a rebalance, consumers can’t consume messages, so a rebalance is basically a short window of unavailability of the entire consumer group. [...]</p>
      <br>
      <p>The way consumers maintain membership in a consumer group and ownership of the partitions assigned to them is by sending <em>heartbeats</em> to a Kafka broker designated as the group coordinator (this broker can be different for different consumer groups). As long as the consumer is sending heartbeats at regular intervals, it is assumed to be alive, well, and processing messages from its partitions. Heartbeats are sent when the consumer polls (i.e., retrieves records) and when it commits records it has consumed.</p>
      <p>If the consumer stops sending heartbeats for long enough, its session will time out and the group coordinator will consider it dead and trigger a rebalance. If a consumer crashed and stopped processing messages, it will take the group coordinator a few seconds without heartbeats to decide it is dead and trigger the rebalance. During those seconds, no messages will be processed from the partitions owned by the dead consumer. When closing a consumer cleanly, the consumer will notify the group coordinator that it is leaving, and the group coordinator will trigger a rebalance immediately, reducing the gap in processing. [There are] configuration options that control heartbeat frequency and session timeouts and [that can be] set to match your requirements. [In recent Kafka versions, a separate heartbeat thread was introduced, that will send heartbeats in between polls as well.]</p>
      <hr>
   </body>
</html>

