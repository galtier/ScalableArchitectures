"""
proxy.py

author: Virginie Galtier
creation date: 02/02/2024

description: 
This scripts subscribes to the Wikimedia stream that published change events,
and prints them.
It also provides a function to extract the country code from the concerned wikimedia domain.

requirements:
- requests
- sseclient-py
"""
import json
import requests
import sseclient

"""
A typical change event looks like that:

{
  "$schema": "/mediawiki/recentchange/1.0.0",
  "meta": {
    "uri": "https://en.wikipedia.org/wiki/Draft:God_Committees",
    "request_id": "646fdd3c-cad7-4edf-b151-fc08bb66a308",
    "id": "62ef509a-775b-4a5c-853b-e1633dbaf1a5",
    "dt": "2024-02-02T15:44:07Z",
    "domain": "en.wikipedia.org",
    "stream": "mediawiki.recentchange",
    "topic": "codfw.mediawiki.recentchange",
    "partition": 0,
    "offset": 988628489
  },
  "id": 1730053651,
  "type": "edit",
  "namespace": 118,
  "title": "Draft:God Committees",
  "title_url": "https://en.wikipedia.org/wiki/Draft:God_Committees",
  "comment": "citation fixes",
  "timestamp": 1706888647,
  "user": "Edcous",
  "bot": false,
  "notify_url": "https://en.wikipedia.org/w/index.php?diff=1202361202&oldid=1202360347",
  "minor": false,
  "length": {
    "old": 26689,
    "new": 26628
  },
  "revision": {
    "old": 1202360347,
    "new": 1202361202
  },
  "server_url": "https://en.wikipedia.org",
  "server_name": "en.wikipedia.org",
  "server_script_path": "/w",
  "wiki": "enwiki",
  "parsedcomment": "citation fixes"
}

This function extracts the 'domain' value from the 'meta' element
(it typically looks like "en.wikipedia.org").
If the change concerns the wikipedia project,
it extracts the first part of that domain (before the first dot sign),
and if the result is a 2-characters long it returns it,
otherwise it returns None
"""
def extract_country_code(change_event):
    try:
        domain_value = change_event["meta"]["domain"]
        if 'wikipedia' in domain_value:
          country = domain_value.split('.',1)[0]
          #  sometimes the domain is prefixed with 'www','simple','commons','incubator','test'...
          if len(country) == 2:
            return country
    except:
        pass


"""
More info about this stream:
https://wikitech.wikimedia.org/wiki/Event_Platform/EventStreams
"""
url = 'https://stream.wikimedia.org/v2/stream/recentchange'
changes = sseclient.SSEClient(url)

for e in changes:
  if isinstance(e, sseclient.Event):
    try:
      change = json.loads(e.data)

      # pretty-print the change with a 2-space indent:
      change_formatted_json = json.dumps(change, indent=2)
      print(change_formatted_json)

      # if the wikimedia domain contains a country code, print it:
      country = extract_country_code(change)
      if country:
        print(country)
    except:
      pass

